#!/usr/bin/env python
import os;
import numpy as np
import math
import sys

num_proc = 1 
num_core_pernode = 4
num_node = 16 
samples = 1

fastest_region = []

def total_slack(a):
  total = 0
  for instance, value in barrier_list.iteritems():
    sum = 0
    for ts in value:
      words = ts.split()
      if len(words) > 1:
        sum = sum + int(words[1]) 
    total = total + sum
  return total

def Initialize_array(a):
   for i in range(proc_num):
     a.append('0')

  #t1_splits = str1.split()
  #t2_splits = str2.split()
  #t1_sec = int(t1_splits[0]) % 10000
  #t1_usec =int( float(t1_splits[1])/100000 )
  #t2_sec = int(t2_splits[0]) % 10000
  #t2_usec =int( float(t2_splits[1])/100000 )

def index_of_time(time1, time2):
  precision = 1000000/samples
  t1_sec = (time1 / 1000000) 
  t1_usec =(time1 % 1000000) / precision 
  t2_sec = (time2 / 1000000) 
  t2_usec =(time2 % 1000000) / precision 

  s1 = t1_sec * samples + t1_usec
  s2 = t2_sec * samples + t2_usec
  return s2-s1

def check_power_length(final_power, final_time):
  for node_id in range(num_node):
    start_ts = final_time[node_id][0]
    end_ts = final_time[node_id][1]
    last_id = index_of_time(start_ts, end_ts)
    print len(final_power[node_id]), last_id+1
    if last_id + 1 != len(final_power[node_id]):
      #print 'check the length of power from interface!!!, its correct!!!'
      print 'the length of the power array is not correct!!!'
 
def Next_Timestamp(time1, time2):
  precision = 1000000/samples
  t1_sec = (time1 / 1000000) 
  t1_usec =(time1 % 1000000) / precision 
  t2_sec = (time2 / 1000000) 
  t2_usec =(time2 % 1000000) / precision 
 
  if samples == 1 and t1_sec + 1 == t2_sec:
    return (1,1)
  if samples == 1 and t1_sec + 2 == t2_sec:
    return (t1_sec+1, 1) 
  if t1_sec == t2_sec and t1_usec + 1 == t2_usec:
    return (1,1)
  if t1_sec + 1 == t2_sec and t1_usec == 9 and t2_usec == 0:
    return (1,1)
  # bug here, it lacks another timestamp between them
  if t1_sec == t2_sec and t1_usec + 2 == t2_usec:
    return (t1_sec, t1_usec+1)
  if t1_sec + 1 == t2_sec and t1_usec == 8 and t2_usec == 0:
    return (t1_sec, 9)
  if t1_sec + 1 == t2_sec and t1_usec == 9 and t2_usec == 1:
    return (t2_sec, 0)
  #a new format
  print 'not recognizable: {0} {1}'.format(time1, time2)
  return (-1, -1)

def Load_filelist(dir, file_list):
  #dir = "./last"
  cur = 0
  file_id = 0 

  logfile = '{0}/LOG_{1}.log'.format(dir, cur + file_id ) 
  while( not os.path.isfile(logfile) ):
    file_id = file_id + 1
    logfile = '{0}/LOG_{1}.log'.format(dir, cur + file_id ) 
  #print 'the first row' 

  for i in range(num_proc):
    lfile = open(logfile, 'r')
    file_list.append( lfile )
    logfile = '{0}/LOG_{1}.log'.format(dir, i + cur + file_id + 1) 
    if len(file_list) == num_proc: break
    while( not os.path.isfile(logfile) ):
      #print logfile
      if i+cur+file_id > 99999: break
      cur = cur + 1
      logfile = '{0}/LOG_{1}.log'.format(dir, i + cur + file_id + 1) 
    if i+cur+file_id > 99999: break

def Load_power(final_power, final_time, dir, id):
  time_array = []
  power_array = []
  for i in range(num_node):
    time_array.append([])
    power_array.append([])
  file_id = id

  for node_id in range(num_node):
    powerfile = '{0}/teller{1}.out'.format(dir, node_id+1+file_id)
    if not os.path.isfile(powerfile) :
      print 'power log file does not exit!!!'
    with open(powerfile) as pf:
      pf = pf.readlines()
 
    ts = 0
    tv = 0
    atv = 0
    ap = 0
    count = 0
    temp_ts = -1
    for line in pf:
      #line = line.replace(',', '')
      power_words = line.split()
      if (len(power_words) == 20): continue
      port = int(power_words[4])
      if port > 0 and port < 6:
        ts = int(power_words[2])
        bug_tv = int(power_words[3])
        count += 1
        atv += bug_tv 
        ap += float(power_words[7])

        if temp_ts == -1: temp_ts = ts
        #there is bug for the usec part, it reachs to the next second
        if temp_ts != -1 and bug_tv < 18000 and temp_ts+1 == ts:
          ts = temp_ts
          count -= 1
          atv -= bug_tv
        if temp_ts != -1 and temp_ts != ts:
          print '{0}:  {1}  {2}  {3} {4}'.format(power_words[0], temp_ts, ts, power_words[3], port),
          print '!!!!!{0}  {1}'.format(int(temp_ts)+1, ts)
      elif port == 6:
        if count == 0: print atv
        else: 
          tv = float(atv)/count
          tv = int(tv)
        #time_array[node_id].append("{0} {1}".format(ts, tv))
        time_array[node_id].append(ts*1000000+tv)
        power_array[node_id].append(ap)
        count = 0
        ap = 0
        atv = 0
        temp_ts = -1
    #print 'the previous length of power: {0}'.format(len(power_array[node_id]))
    
    #the power log is not consistent with the number of samples
    time_len = len(time_array[node_id])
    final_time[node_id].append(time_array[node_id][0])
    final_time[node_id].append(time_array[node_id][time_len-1]) 
    final_power[node_id].append(power_array[node_id][0]) 
    
    for i in range(len(power_array[node_id])-1):
      (tsec, tvsec) = Next_Timestamp(time_array[node_id][i], time_array[node_id][i+1]) 
      if tsec == -1: 
        print 'there is more than one element between the gap'
        continue
      if tsec > 1:
        est_power = (power_array[node_id][i] + power_array[node_id][i+1])/2
        final_power[node_id].append(est_power)
      final_power[node_id].append(power_array[node_id][i+1])

def aggregate_power(final_power, final_time, start_array, second_power, num_sec):
  for i in range(num_node):
    second_power.append([])
  
  sec = start_array[0]/1000000
  for p in range(num_proc-1):
    s = start_array[p+1]/1000000
    if s != sec: 
      print 'the starting timestamps of all procs are not on the same second!!!'
      return
  #assuming collecting power data starts at the same time 
  start_ts = final_time[0][0]
  start_ts = start_ts/(1000000/samples)
  sid = start_ts % samples
  sid = (samples - sid) % samples
  offset = sec - start_ts/samples
  if sid != 0:
    offset -= 1
  sid += offset * samples
  for node_id in range(num_node):
    plen = len(final_power[node_id])
    if sid+num_sec*samples > plen:
      print 'there is not enough power data for actual execution'
      return
    for i in range(num_sec):
      sum = 0
      id = sid + i*samples
      for t in range(samples):  
        sum += final_power[node_id][id+t]
      second_power[node_id].append(sum/samples)
 
def fix_region(timestamps, region_array, index_array):
  barrier_count = len(timestamps)/2/num_proc
  region_ts = []
  for k in range(num_proc):
    region_ts.append(0)
  
  id = 0 
  temp_array = []
  while id < len(timestamps):
    for k in range(num_proc):
      start_time = timestamps[id+2*k]
      end_time = timestamps[id+2*k+1]
      temp_array.append(end_time - region_ts[k])
      region_ts[k] = end_time 
    id += 2*num_proc 
  
  sub_array = []
  for k in range(num_proc):
    sub_array.append(0)

  id = 0
  for s in range(len(index_array)):
    eid = index_array[s]
    while id <= eid:
      for k in range(num_proc):
        sub_array[k] += temp_array[id + k]
      id += num_proc 
    for k in range(num_proc): 
      region_array.append(sub_array[k])
      sub_array[k] = 0

def region_time(timestamps, region_array, ptime, index_array):
  id = 0
  region_ts = [] 
  for k in range(num_proc):
    region_ts.append(0)
  
  temp_array = []
  while id < len(timestamps):
    for k in range(num_proc):
      start_time = timestamps[id+2*k]
      end_time = timestamps[id+2*k+1]
      temp_array.append(end_time - region_ts[k])
      region_ts[k] = end_time 
    id += 2*num_proc 
  
  id = 0
  while id < len(temp_array):
    t_time = 0
    i = id
    #using the first process' region
    t_time += temp_array[i]
    i = i + num_proc
    while t_time <= ptime and i < len(temp_array):
      t_time += temp_array[i]
      i = i + num_proc
    #aggregate one region
    index_array.append(i-num_proc)
    
    for j in range(num_proc):
      temp = 0
      start_id = id + j
      while start_id < i and start_id < len(temp_array):
         temp += temp_array[start_id]
         start_id += num_proc 
      region_array.append(temp)
    id = i
  #print  len(region_array)

def interval_time(timestamps, interval_array):
  id = 0 
  barrier_count = len(timestamps)/(2*num_proc) 

  while id < len(timestamps):
    for k in range(num_proc):
      end_id = id + 2*k + 1
      next_start_id = id + 2*num_proc + 2*k
      if next_start_id >= len(timestamps): break
      end_time = timestamps[end_id]
      next_start_time = timestamps[next_start_id]
      interval_time =  next_start_time - end_time
      interval_array.append(interval_time)
    id += 2*num_proc
  interval_count = len(interval_array)/num_proc
  if interval_count + 1 != barrier_count:
    print 'count is wrong!!!'
    print 'the count is {0} {1} ...'.format(interval_count, barrier_count)
  else:
    print 'processing is correct'

slack_out = 0
def slack_time(timestamps, slack_array):
  id = 0 
  ms_count = 0
  s_count = 0
  f_count = 0
  os_count = 0
  t_count = 0
  fo_count = 0
  barrier_count = 0

  while id < len(timestamps):
    for k in range(num_proc):
      start_time = timestamps[id+2*k]
      end_time = timestamps[id+2*k+1]
      slack_time = end_time - start_time
      slack_array.append(slack_time)
      if (slack_time > 1000): ms_count += 1  
      if (slack_time <= 80): f_count += 1  
      if (slack_time <= 50): s_count += 1  
      if (slack_time <= 60): t_count += 1  
      if (slack_time <= 70): fo_count += 1  
      if (slack_time > 1000000): 
        os_count += 1 
        #print barrier_count, slack_time 
    id += 2*num_proc
    barrier_count += 1
  if slack_out == 1: 
    print 'the slack time that is bigger than 1ms {0}%'.format(float(ms_count)*100/float(barrier_count)/16)
    print 'the slack time that is bigger than 50us {0}%'.format(float(s_count)*100/float(barrier_count)/16)
    print 'the slack time that is bigger than 60us {0}%'.format(float(t_count)*100/float(barrier_count)/16)
    print 'the slack time that is bigger than 70us {0}%'.format(float(fo_count)*100/float(barrier_count)/16)
    print 'the slack time that is bigger than 80us {0}%'.format(float(f_count)*100/float(barrier_count)/16)
    print 'the slack time that is bigger than 1s {0}%'.format(float(os_count)*100/float(barrier_count)/16)

def writing_interval_file(slack_array, interval_array):
  fname = "{0}/interval.log".format(dir)
  ifile = open(fname, "w")
  fname = "{0}/slack.log".format(dir)
  sfile = open(fname, "w") 
 
  interval_len = len(interval_array)/num_proc
  if interval_len + 1 != len(slack_array)/num_proc:
    print 'interval is not consistent with slack'
    return 
  id = 0
  instance = 0
  while id < len(slack_array): 
    slack_str = ''
    for k in range(num_proc):
      slack_str += '{0}\n'.format(slack_array[id + k])
    sfile.write('{0}'.format(slack_str)) 
    if instance < interval_len:
      interval_str = '' 
      for k in range(num_proc):
        interval_str += '{0}\n'.format(interval_array[id + k])
      ifile.write('{0}'.format(interval_str))
    id += num_proc
    instance += 1 
  sfile.close()
  ifile.close()

def writing_file(timestamps, slack_array, region_array):
  fname = "{0}/timestamp.log".format(dir)
  tfile = open(fname, "w")
  fname = "{0}/exit_time.log".format(dir)
  efile = open(fname, "w")
  fname = "{0}/slack_time.log".format(dir)
  slackfile = open(fname, "w") 
  fname = "{0}/region_time.log".format(dir)
  regionfile = open(fname, "w") 
  
  id = 0
  if len(slack_array) != len(region_array):
    print 'there is some inconsistency between those two arrays!!!'
  instance = 0
  while id < len(slack_array): 
    region_str = '' 
    slack_str = ''
    for k in range(num_proc):
      start_time = timestamps[id+2*k]
      end_time = timestamps[id+2*k+1]
      region_str += '{0: >9} '.format(region_array[id + k])
      slack_str += '{0: >9} '.format(slack_array[id + k])
    slackfile.write('{0: >5}: {1}\n'.format(instance, slack_str)) 
    regionfile.write('{0: >5}: {1}\n'.format(instance, region_str))
    id += num_proc
    instance += 1 
   
  instance = 0
  id = 0 
  while id < len(timestamps):
    t1_str = ''
    t2_str = ''
    for k in range(num_proc):
      start_time = timestamps[id+2*k]
      end_time = timestamps[id+2*k+1]
      t1_str += '{0: >9} '.format(start_time)
      t2_str += '{0: >9} '.format(end_time)
    tfile.write('{0: >5}: {1}\n'.format(instance, t1_str))
    tfile.write('{0: >5}  {1}\n'.format('', t2_str))
    
    end_str = ''
    exit_time = []
    for k in range(num_proc):
      exit_time.append(timestamps[id+2*k+1])
    total_max = exit_time[0] 
    total_min = exit_time[0] 
    for k in range(num_proc/num_core_pernode):
      start_id = k*num_core_pernode
      mint = exit_time[start_id]
      maxt = exit_time[start_id]
      idi = start_id
      ida = start_id 
      for t in range(num_core_pernode-1):
        tid = start_id + t + 1
        if exit_time[tid] > maxt:
          ida = tid 
          maxt = exit_time[tid]
        if exit_time[tid] < mint:
          idi = tid 
          mint = exit_time[tid]
      end_str += '{0} - {1} : {2}  '.format(ida, idi, maxt-mint)
      if maxt > total_max:
        total_max = maxt
      if mint < total_min:
        total_min = mint
    end_str += ' all processes {0}'.format(total_max - total_min)
    efile.write('{0}\n'.format(end_str))
    id += 2*num_proc 
    instance += 1 
  tfile.close()
  efile.close()
  slackfile.close()
  regionfile.close()

def analysis_log(file_list, timestamps, start_array):
  j = 0
  log_line = file_list[j].readline()
  total_E = 0
  main_start = 0
  
  key_dict = []
  for k in range(num_proc):
    my_dict = {}
    key_dict.append(my_dict)
 
  rank_list = [] 
  while j < num_proc:
    log_line = log_line.replace(',', '')
    log_words = log_line.split()
    word_len = len(log_words)
    if word_len == 4:
      main_start = int(log_words[0])*1000000 + int(log_words[1])
      log_rank = int(len(log_words)-1)
      start_array.append(main_start)
      rank_list.append(log_rank)
      j += 1
    else: 
      print 'the first line of log file is not the starting timestamp!!!!'
    if j < num_proc:
      log_line = file_list[j].readline()
  
  for i in range(num_proc):
    for t in range(num_proc):
      if i == rank_list[t]: 
        if t == i: break
        temp = file_list[i]
        file_list[i] = file_list[t]
        file_list[t] = temp
        temp = rank_list[i]
        rank_list[i] = rank_list[t]
        rank_list[t] = temp
        break    
  #print file_list
  j = 0
  log_line = file_list[j].readline()
  while log_line :  
    start_time = []
    end_time = []
    while j < num_proc:
      #print log_line
      log_line = log_line.replace(',', '')
      log_words = log_line.split()
      word_len = len(log_words)
      instance = int(log_words[0])
      context_key = log_words[1]
      ts1 = int(log_words[word_len-2])
      ts2 = int(log_words[word_len-1])
      
      timestamps.append(ts1)
      timestamps.append(ts2)
      if (ts2  < 400000000):
        if context_key in key_dict[j]:
          key_dict[j][context_key] += 1
        else:
          key_dict[j][context_key] = 1 
      j += 1
      if j < num_proc: 
        log_line = file_list[j].readline()
    j = 0
    log_line = file_list[j].readline()
  if len(start_array) != num_proc:
    print start_array
    print 'the starting timestamp is not consistent with the process number!!!'
  #for k in range(num_proc):
    #print 'the number of context key in proc {0}, is {1}'.format(k, len(key_dict[k].keys()))

def get_region_energy(start_ts, region_dur, final_power, final_time, node_id):
  end_ts = start_ts + region_dur
  sec = 1000000
  precision = 1000000/samples 
  
  power_len = len(final_power[node_id])
  max_time = final_time[node_id][0] + (power_len-1) * precision 
  if end_ts > max_time:
    print 'exceeds!!!!!  power file max time: {0} the actual region time: {1}'.format(max_time, end_ts)
  log_index = int(start_ts / precision)
  start_index = int(final_time[node_id][0] / precision)
  #print 'the requestest starting time is:{0}  the power file starts: {1}'.format(start_ts/sec, final_time[node_id][0]/sec) 
  id = log_index - start_index
  power = final_power[node_id][id]

  its = (log_index + 1) * precision
  sub_time = 0
  if end_ts < its: 
    return power*float(region_dur)/sec
  else:
    sub_time = its - start_ts 
  
  energy = 0
  while its <= end_ts:
    e = float(sub_time)/sec * power 
    energy += e
    id += 1
    power = final_power[node_id][id]
    sub_time = precision
    its += precision
  sub_time = end_ts % precision
  energy += float(sub_time)/sec * power
  return energy
     

def total_energy(timestamps, start_array, final_power, final_time):
  assert num_proc == len(start_array)
  proc_pernode = num_core_pernode
  
  tlen = len(timestamps)
  num_barrier = tlen/num_proc/2
  id = num_barrier - 1
  id = id * num_proc * 2
  end_ts = []
  
  for k in range(num_proc):
    id += 1
    end_ts.append(timestamps[id])
    id += 1

  #for test cases 
  lenp = num_proc/num_core_pernode
  num = num_node
  if lenp > num_node:
    num = lenp
  if num_proc < proc_pernode:
    proc_pernode = num_proc

  sum_energy = 0
  for k in range(num):
    for s in range(proc_pernode):
      id = k * proc_pernode + s
      if proc_pernode == 1: id = 0
      start_ts = start_array[id]
      rtime = end_ts[id]
      energy = get_region_energy(start_ts, rtime, final_power, final_time, k)    
      sum_energy += energy 
  return sum_energy

def check_time(timestamps, region_array):
  tlen = len(region_array)
  num_barrier = tlen/num_proc
  proc_pernode = num_proc / num_node

  whole_region = []
  for s in range(num_proc):
    sum = 0
    for k in range(num_barrier):
      region_dur = region_array[k*num_proc + s]
      sum += region_dur
    whole_region.append(sum)
  
  id = num_barrier - 1
  id = id * num_proc * 2
  end_ts = []

  for k in range(num_proc):
    id += 1
    end_ts.append(timestamps[id])
    id += 1 
  
  for k in range(num_proc):
    if whole_region[k] != end_ts[k]:
      print 'timestamps is not consistent with region_array {0}  {1}'.format(whole_region[k], end_ts[k])

def slack_energy(slack_array, region_array, energy_array, stime):
  lens = len(slack_array)
  lenr = len(region_array)
  lene = len(energy_array)
  assert lens == lenr
  assert lenr == lene
  num_barrier = lens/num_proc
  
  sum_e = 0
  for e in energy_array:
    sum_e += e
  print 'the total e: {0}'.format(sum_e)

  ep = 0.19/2.8  
  saved_energy = 0
  slack_time = 0
  count = 0
  for k in range(num_barrier):
    for s in range(num_proc):
      slack = slack_array[k*num_proc + s]
      if slack > stime:
        duration = region_array[k*num_proc + s] 
        slack_time += slack
        if slack > duration:
          print 'bug:::: slack time is longer than its length!!!!'
        per = duration/(duration-slack)
        if per > 1: per = 1
        per = per * ep
        saved_energy += per*energy_array[k*num_proc + s]
        count += 1
  print 'total slack time is {0}, the energy saved by slack-based way is {1}, switching count {2}'.format(slack_time/num_proc, saved_energy, count)
        
def region_energy(region_array, start_array, final_power, final_time, energy_array):
  tlen = len(region_array)
  num_barrier = tlen/num_proc
  proc_pernode = num_core_pernode
 
  region_ts = []
  assert num_proc == len(start_array)
  for k in range(len(start_array)):
    region_ts.append(start_array[k])

  for k in range(num_barrier):
    for s in range(num_proc):
      region_dur = region_array[k*num_proc + s]
      start_ts = region_ts[s]
      node_id = s/proc_pernode
      energy = get_region_energy(start_ts, region_dur, final_power, final_time, node_id)
      energy /= proc_pernode
      energy_array.append(energy) 
      region_ts[s] = start_ts + region_dur 

def single_region_energy(region_array, start_array, final_power, final_time, energy_array):
  tlen = len(region_array)
  num_barrier = tlen/num_proc
  proc_pernode = num_core_pernode
 
  region_ts = []
  assert num_proc == len(start_array)
  for k in range(len(start_array)):
    region_ts.append(start_array[k])

  sum_t = 0
  for t in region_array:
    sum_t += t
  print 'the region length {0},  all time {1}'.format(tlen, float(sum_t)/1000000)
  mt = region_ts[0] + sum_t
  power_len = len(final_power[0])
  max_time = final_time[0][0] + (power_len-1) * 1000000 
  if mt > max_time:
    print 'the stupid time {0} > {1}'.format(mt, max_time)

  for k in range(num_barrier):
    barrier_energy = 0
    region_dur = region_array[k*num_proc]
    start_ts = region_ts[0]
    for s in range(num_node):
      node_id = s
      energy = get_region_energy(start_ts, region_dur, final_power, final_time, node_id)
      barrier_energy += energy
    region_ts[0] = start_ts + region_dur 
    energy_array.append(barrier_energy) 

def standard_deviation(array):
  mylen = len(array)
  sum = 0
  for element in array:
    sum += float(element)
  avg = sum/float(mylen)
  sd = 0
  for element in array:
    sd += math.pow((avg - element), 2)
  sd = sd/float(mylen)
  sd = math.sqrt(sd) 
  return (avg, sd)

def get_region_id(start_sec, end_sec, no_barrier):
  start_id = 0
  for i in range(start_sec):
    start_id += no_barrier[i][1]
  end_id = 0
  for i in range(end_sec+1):
    end_id += no_barrier[i][1]
  return (start_id, end_id)

def write_barrier_density(dir, id, no_barrier, second_power):
  fname = "{0}/{1}.out".format(dir, id)
  freqfile = open(fname, "w")
  for i in range(len(no_barrier)):
    sum_power = 0
    for node_id in range(num_node):
      sum_power += second_power[node_id][i] 
    freqfile.write('{0} {1}\n'.format(no_barrier[i][1], sum_power))
  freqfile.close() 

def regions_duration(start_id, end_id, region_array):
  sum_time = 0
  barrier_len = len(region_array)/num_proc
  
  assert start_id >= 0
  assert end_id < barrier_len
  id = start_id
  while id <= end_id:
    pi = id * num_proc
    sum_time += region_array[pi]
    id += 1
  return sum_time

def regions_energy(start_id, end_id, energy_array):
  sum_energy = 0
  barrier_len = len(energy_array)/num_proc
  
  assert start_id >= 0
  assert end_id < barrier_len
  id = start_id
  while id <= end_id:
    pi = id * num_proc
    sum_energy += energy_array[pi]
    id += 1
  return sum_energy

def density_barrier(timestamps, start_array, no_barrier):
  sec_array = []
  barrier_len = len(timestamps)/2/num_proc
  for s_time in start_array:
    sec_array.append(s_time/1000000)
  time1 = sec_array[0]
  for i in range( len(sec_array)-1 ):
    if time1 != sec_array[i+1]:
      print 'the process {0} starts at a different !!!'.format(i+1)

  i = 0
  pre_time = timestamps[0]/1000000
  cur_sec = 1 
  count = 1
  while i+2*num_proc < len(timestamps):
    i += 2*num_proc
    cur_time = timestamps[i]/1000000 
    if pre_time != cur_time:
      while cur_sec < pre_time:
        no_barrier.append((cur_sec, 0))
        cur_sec += 1
      no_barrier.append((pre_time, count))
      pre_time = cur_time
      count = 1
      cur_sec += 1 
    else: count += 1
  
  while cur_sec < pre_time:
    no_barrier.append((cur_sec, 0))
    cur_sec += 1
  no_barrier.append((pre_time, count))
  
  sum_len = 0
  for ele in no_barrier:
    sum_len += int(ele[1])
  #print 'total seconds are: {0} !!!'.format(len(no_barrier))

def print_freq_percentage(freq_array):
  fcount = []
  for i in range(num_freq):
    fcount.append(0) 
  for f in freq_array:
    fcount[f] += 1
  print ' frequency percentage: ',
  for i in range(num_freq):
    print '{0} % '.format(float(fcount[i])*100/len(freq_array)),

def print_optimal(freq_region_energy, freq_array):
  good_energy_array = []
  mylen = len(freq_region_energy[0])
 
  b_energy = 0
  for i in range(mylen):
    freq = 0 
    t_energy = freq_region_energy[0][i]
    for f in range(num_freq):
      energy = freq_region_energy[f][i]
      if energy < t_energy:
        t_energy = energy
        freq = f 
    freq_array.append(freq)
    good_energy_array.append(t_energy)
    b_energy +=  t_energy
  print ' the potential energy saving is {0} '.format(b_energy)

def print_max_time(timestamps):
  blen = len(timestamps)
  len_id = blen/2/num_proc - 1
  max_time = 0
  for k in range(num_proc):
    eid = len_id * num_proc * 2 + 1 + 2*k
    if max_time < timestamps[eid]:
      max_time = timestamps[eid]
  max_time = float(max_time) / 1000000
  print '  the total execution time is {0}  total energy is {1},  '.format(max_time, t_energy)
  return max_time 

def noise_detection(freq_region_array, noise_array):
  for i in range(num_freq):
    noise_array.append([])
  for i in range(num_freq):
    for j in range(len(freq_region_array[0])):
      noise_array[i].append(0)
  for i in range(len(freq_region_array[0])):
    fatest = freq_region_array[0][i]
    for j in range(num_freq-1):
      ttt = freq_region_array[j+1][i]
      if ttt < fatest:
        noise_array[j+1][i] = 1
      else:
        fatest = freq_region_array[j+1][i]

if len(sys.argv) > 1:
  num_freq = int(sys.argv[1]) 
else: 
  num_freq = 5 
ntimes = 1 
freq_ntimes = []
#used to aggregate the regions
index_array = []

first_sign = 1 
for t in range(ntimes):
  freq_region_energy = []
  freq_region_time = []
  for fi in range(num_freq):
    #dir = './p{0}/{1}'.format(fi+1, t)
    id =  1 + fi 
    dir = './p{0}'.format(id)
    #dir = '.'
    file_list = []
    start_array = []
    timestamps = []
    print dir,
    
    Load_filelist(dir, file_list) 
    if len(file_list) != num_proc:
      print 'the log file list is not correct!!! file list length {0}'.format(len(file_list))
      break 
    analysis_log(file_list, timestamps, start_array) 
    #print start_array
 
    #power data, time is string
    final_power = []
    final_time = []
    for i in range(num_node):
      final_time.append([])
      final_power.append([])
    pdir = './data-power/power-{0}'.format(id)
    #pdir = './power-data'
    #Load_power(final_power, final_time, pdir, t)
    Load_power(final_power, final_time, pdir, 0)
    #print final_time
    #check_power_length(final_power, final_time)  
    #print final_power[0]
 
    slack_array = []
    region_array = []
    second_power = []   
    energy_array = []
    interval_array = []
 
    slack_time(timestamps, slack_array)
    
    #interval_time(timestamps, interval_array)
    #writing_interval_file(slack_array, interval_array) 
    
    #fix_region is used to fix the barriers to region
    if first_sign == 1:
      region_time(timestamps, region_array, 1000000, index_array)
      first_sign = 0 
    else:
      fix_region(timestamps, region_array, index_array)
    
    freq_region_time.append(region_array)
    sum_time = 0
    for tt in region_array:
      sum_time += tt
    single_region_energy(region_array, start_array, final_power, final_time, energy_array)
    #region_energy(region_array, start_array, final_power, final_time, energy_array)
    freq_region_energy.append(energy_array)
    #print 'the total regions length {0}, time: {1}'.format(len(region_array), float(sum_time)/(num_proc*1000000))
    #writing_file(timestamps, slack_array, region_array) 
   
    energy_sum = 0
    for e in energy_array:
      energy_sum += e 
    #print 'energy array {0}'.format(energy_sum) 
    t_energy = total_energy(timestamps, start_array, final_power, final_time)
    max_time = print_max_time(timestamps)
    #slack_energy(slack_array, region_array, energy_array, 600000)    
    #print start_array
    #check_time(timestamps, region_array)
    #sys.exit() 
    
    no_barrier = []
    density_barrier(timestamps, start_array, no_barrier)
    aggregate_power(final_power, final_time, start_array, second_power, int(max_time))
    print len(second_power[0]), len(no_barrier)
    if fi == 0:
      write_barrier_density('.', fi, no_barrier, second_power)
    continue
     
    (sid, eid) = get_region_id(10, 14, no_barrier)
    print (sid, eid)
    rtime = regions_duration(5494, 16700, region_array)   
    renergy = regions_energy(5494, 16700, energy_array)
    print '***check the duration: {0} energy {1}'.format(rtime, renergy)
    
    p_time = max_time
    p_energy = t_energy
    
  o_energy = 0
  o_time = 0
  first_time = 0
  freq_array = []
  noise_array = []
  noise_detection(freq_region_time, noise_array)
  noise_count = 0
 
  for si in range(len(freq_region_energy[0])):
    se = freq_region_energy[0][si]
    st = freq_region_time[0][si]
    freq_id = 0
    for fi in range(num_freq):
      #if noise_array[fi][si] == 1:
      #  freq_id = 0
      #  noise_count += 1
      #  break
      if noise_array[fi][si] != 1 and freq_region_energy[fi][si] < se:
      #if freq_region_energy[fi][si] < se:
        se = freq_region_energy[fi][si]
        freq_id = fi
    o_energy += se
    freq_array.append(freq_id)
    o_time += freq_region_time[freq_id][si] 
  print 'the length of freq is {0}'.format(len(freq_array)/num_proc)
  print 'count', noise_count 
  #print freq_array
  for f in range(len(freq_array)):
    if freq_array[f]-1 >= 0: freq_array[f] -= 1
  #print freq_array
  f_result = open('./freq.txt', 'w')
  pid = 0
  pre_f = freq_array[pid]
  f_result.write('{0} {1}\n'.format(0, pre_f))
  pid += 1
  while pid<len(freq_array):
    for pi in range(num_proc):
      if freq_array[pid] != pre_f:
        f_result.write( '{0} {1}\n'.format(index_array[pid-1]+1, freq_array[pid]) )
        pre_f = freq_array[pid]
      pid += 1
  f_result.close()
  print '\nthe optimal energy should be {0}, time {1}'.format(o_energy, o_time) 
  print 'the length of index array {0}'.format(len(index_array))
  #freq_array = []
  #print_optimal(freq_region_energy, freq_array)
  #freq_ntimes.append(freq_array)
  energy_result = open('./energy.txt', 'w')
  for si in range(len(freq_region_energy[0])):
    if si - 1 < 0:
      instance = 0
    else: instance = index_array[si-1] + 1
    energy_result.write('{0} '.format(instance))
    for fi in range(num_freq):
      #if noise_array[fi][si] != 1 and freq_region_energy[fi][si] < se:
      energy_result.write( '{0} '.format(freq_region_energy[fi][si]) )
    energy_result.write( '\n' ) 
  energy_result.close()


  

